
# Intro

This project uses SaltStack, test-kitchen, vagrant, VirtualBox for deploy web app infrastructure
configuration.

The project is tested on Debian-9 hosts.

Monitoring host is not provisioned and SaaS applications used (see e-mail)

# Install requirements for test-kitchen

All instructions in this section are using super privileges

Download and install virtualbox with kernel modules
```
wget http://download.virtualbox.org/virtualbox/5.2.0/virtualbox-5.2_5.2.0-118431~Debian~stretch_amd64.deb"
apt install linux-headers-amd64 gcc make perl
dpkg -i virtualbox-5.2_5.2.0-118431~Debian~stretch_amd64.deb
apt-get install -f

```

Add user to vboxusers
```
usermod -a -G vboxusers admin
```

Install ruby and project requirements

```
apt install ruby git ruby2.3-dev g++
```

Install vagrant

```
wget https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.deb
dpkg -i vagrant_2.0.1_x86_64.deb
```

# Install project

All instructions in this section are using user privileges

Clone the project
```
git clone git@bitbucket.org:maratsh/kredits.git
cd kredits
```

Install test-kitchen with plugins
Run in project folder

```
bundle install --path vendor/bundle
```

Configure ruby gem folder path
Run and add these lines to ~/.bashrc
```
export PATH=$PATH:vendor/bundle/ruby/2.3.0/bin/
export GEM_HOME=vendor/bundle/ruby/2.3.0/
```

# Deploy project

Deploy project VM's
In project folder run this command as user

```
kitchen converge
```

# Configure network on host

All instructions in this section are using super privileges

For accepting 80 and 443 incoming requests we should configure REDIRECT to 8080 and 4430 kitchen
forwarded ports. Kitchen forwards from a host-only adapter. ports with vagrant "forwarded port" feature.
Also, we should configure firewall and accept web and ssh connections only.

Install the iptables config daemon

```
apt install  netfilter-persistent
```

Edit iptables config file /etc/iptables/rules.v4

```
*nat
-A PREROUTING -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 8080
-A PREROUTING -p tcp -m tcp --dport 443 -j REDIRECT --to-ports 4430
COMMIT
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [21125305:12005054022]
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 8080 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 4430 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
-A FORWARD  -p tcp --dport 8080  -j ACCEPT
-A FORWARD  -p tcp --dport 4430  -j ACCEPT
-A FORWARD -j REJECT --reject-with icmp-port-unreachable
COMMIT
```

Apply new config
```
iptables-restore <  /etc/iptables/rules.v4
```

Check app web interface
