# server
```
Think of ways on how the performance of the application could be enhanced on proxy-server
level.
```
* cache static (js,html)
* cache some backend answers by keys like $uri$args$http_user_login
* Network stack fine tuning (tcp buffers in example)
* Nginx socket and workers tuning  (for example reuseport in listen)
* Just use CDN! =)
* use http2


```
Think of ways on how to create a redundant setup for the proxy-machine
```

* Use 2 balancers with VIP's. Route traffic by RRDNS and keepalived
* Geo-redundancy: Use groups of balancers with VIP in different locations and route traffic between by BGP and RRDNS
* AWS: ALB's + Smart RRDNS from Route53


# Application profiling

All information digged with Chrome Dev console

# Measures
Loading time is 2.6 seconds for me. It's not bad, but not great result. It's can affect conversion.
As I can see too much time is spent on scripting (see knet.png.)

## visitor tracking

A lot of tracking tools

* Piwik (broken https !)
* Yandex
* google
* AB TASTY
* NewRelic
* FaceBook
* and others ?)


You are using in-house tracking tool Piwik.
The certificate is expired a month ago, so it's not working because js can't be loaded.
In chrome that caused RED "Not secure" plate on the site. It's very bad for conversion.
Admin page is open to the world https://tracking.kreditech.com/login
It's dangerous because you can lose tons of users data to evil people.

My suggestion based on my own expirience is not to use Piwik for public projects at all because that thing is very slow and vulnerable.
If you want to use Piwik:
* use certs auto-update feature by AWS or LE
* store piwik.js on project side, behind the CDN
* Do not expose login page (index.php), expose only tracking piwik.php

##CDN

You are using Cloudflare as CDN. It's a great optimisation, but not for Russia, because Cloudflare has only one dc in Moscow.
Latency in Russia can be very different by region because of long distances, not good infrastructure and strange routing through other countries.
(see ping.png)
Latency for your site for Russia can be between 20(Central and Volga regions) to 300ms (Syberia and Pacific)
The solution is to use another CDN with a more wide presence or to create caching proxy in most interesting regions by yourselves.

##JS

Framework: backbone and jquery

You have too many js files. Some of them is minified, other is not.

```
https://www.kredito24.ru/js/client/common_lean.min.js?v=1002.0.40.867392
https://www.kredito24.ru/js/client/translation/kredito_ru_RU.js?v=1002.0.40.867392
https://www.kredito24.ru/js/client/vendor_lean.min.js?v=1002.0.40.867392
https://www.kredito24.ru/js/conversion/jquery.scrollex.min.js?v=1002.0.40.867392
https://www.kredito24.ru/js/conversion/jquery.scrolly.min.js?v=1002.0.40.867392
https://www.kredito24.ru/js/conversion/main.js?v=1002.0.40.867392
https://www.kredito24.ru/js/conversion/skel.min.js?v=1002.0.40.867392
https://www.kredito24.ru/js/plugins/jquery.flexslider.js
https://www.kredito24.ru/js/single/pages/main/fixed_header.js
```


## Backend

Because all requests are handled by CDN it's hard to say what runs on the backend and where is the Backend.
I tried to disclose location by e-mail headers, but form on your site is not working.
