include:
  - nginx.ng.pkg
  - nginx.ng.service

/etc/nginx/conf.d/default.conf:
  file.managed:
    - contents: |
       # disabled
    - watch_in:
      - service: nginx


/etc/nginx/conf.d/status.conf:
  file:
    - managed
    - watch_in:
      - service: nginx
    - contents: |
        server {
          listen 127.0.0.1:10000;
          location /server_status {
            stub_status;
          }
         }
