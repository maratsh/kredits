{% from "kweb/map.jinja" import kweb with context %}

include:
  - nginx
  - php.ng
  - php.ng.fpm

# deploy our "application"
/var/www/kredits:
  file:
    - recurse
    - source: salt://kweb/files/app

nginx-user:
  user.present:
    - name: nginx
    - groups:
      - www-data
    - watch_in:
      - service: nginx


/etc/nginx/conf.d/kredits.conf:
  file:
    - managed
    - template: jinja
    - source: salt://kweb/files/nginx.conf
    - context:
      c: {{ kweb }}
    - watch_in:
      - service: nginx
