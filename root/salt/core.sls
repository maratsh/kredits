core:
  pkg:
    - installed
    - refresh: True
    - order: 2
    - pkgs:
      - sudo:
      - vim:
      - rsync
      - screen
      - git:
      - git-core:
      - git-man:
      - curl:
      - man
      - mosh
      - htop
      - aptitude
      - ifstat
      - atop
      - strace
      - xfsprogs
      - dstat

atop:
  service:
    - running
    - enable: True
