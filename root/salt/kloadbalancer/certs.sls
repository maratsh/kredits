{% from "kloadbalancer/map.jinja" import kloadbalancer with context %}

/etc/nginx/ssl/:
  file:
    - directory
    - user: root

/etc/nginx/ssl/{{ kloadbalancer.domain }}.crt:
  file:
    - managed
    - template: jinja
    - mode: 0600
    - user: root
    - group: root
    - show_changes: False
    - makedirs: True
    - contents_pillar: kloadbalancer:https:crt

/etc/nginx/ssl/{{ kloadbalancer.domain }}.key:
  file:
    - managed
    - template: jinja
    - mode: 0600
    - user: root
    - group: root
    - show_changes: False
    - makedirs: True
    - contents_pillar: kloadbalancer:https:key

gendhparam:
  cmd:
    - run
    - name: openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2048
    - creates: /etc/nginx/ssl/dhparam.pem
    - require:
      - file: /etc/nginx/ssl/
