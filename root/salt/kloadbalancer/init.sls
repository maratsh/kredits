{% from "kloadbalancer/map.jinja" import kloadbalancer with context %}

include:
  - nginx
  - kloadbalancer.certs



/etc/nginx/conf.d/kredits.conf:
  file:
    - managed
    - template: jinja
    - source: salt://kloadbalancer/files/nginx.conf
    - context:
      c: {{ kloadbalancer }}
    - watch_in:
      - service: nginx
    - require:
      - file: /etc/nginx/ssl/*
      - cmd: gendhparam
