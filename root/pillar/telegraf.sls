telegraf:
  version: 1.4.4-1
  source_hash: 4094fd91d20814ac6a6b13a78fe53aaa
  indent: 2
  use_system_inputs: True
  agent:
    hostname: {{ grains['id'] }}
    interval: 1s
    round_interval: "true"
    metric_buffer_limit: 10000
    flush_buffer_when_full: "true"
    collection_jitter: 0s
    flush_interval: 10s
    flush_jitter: 0s
    debug: "false"
    quiet: "false"
  outputs:
    -
      plugin_name: influxdb
      urls:
        - https://delorean-aa8d0a08.influxcloud.net:8086
      database: telegraf
      precision: s
      username: bojubFeg5
      password: bojubFeg5
  inputs:
    -
      plugin_name: nginx
      urls:
        - http://127.0.0.1:10000/server_status
    -
      plugin_name: net
      interfaces:
        - enp0s3
        - enp0s8
