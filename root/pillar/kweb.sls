nginx:
  ng:
    install_from_repo: true
    service:
      enable: True
      opts: {}

kweb:
  domain: zaimo.ru
  realip_from: 192.168.255.0/24

php:
  ng:
    fpm:
      pools:
        'www.conf':
          enabled: True
          settings:
            www:
              user: www-data
              group: www-data
              listen: /run/php/php7.0-fpm.sock
              listen.owner: www-data
              listen.group: www-data
              pm: dynamic
              pm.max_children: 5
              pm.start_servers: 2
              pm.min_spare_servers: 1
              pm.max_spare_servers: 3
