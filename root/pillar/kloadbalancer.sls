nginx:
  ng:
    install_from_repo: true
    service:
      enable: True
      opts: {}

kloadbalancer:
  domain: zaimo.ru
  upstreams:
    - 192.168.255.14
    - 192.168.255.13
    - 192.168.255.2 #dead host
  https:
    crt: |
      -----BEGIN CERTIFICATE-----
      MIIDQzCCAiugAwIBAgIJAKQj89s7TV56MA0GCSqGSIb3DQEBCwUAMDgxFTATBgNV
      BAMMDGFydWxqb2huLmNvbTESMBAGA1UECgwJQXJ1bCBKb2huMQswCQYDVQQGEwJV
      UzAeFw0xNzExMTEwODA4MzNaFw0xODExMTEwODA4MzNaMDgxFTATBgNVBAMMDGFy
      dWxqb2huLmNvbTESMBAGA1UECgwJQXJ1bCBKb2huMQswCQYDVQQGEwJVUzCCASIw
      DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMsMt11RUD5U9TutmaoJHuLZX+xQ
      lDAkEvU/oHO1CG2807fbU5deORurgVIEP3/t3rJg1yoDqLxOuh1j1Gk6JD66rliq
      BQZBP8a0eB7ktsqa8W8IO1Qd1O4AnDT/+CCAlPHsktDVgbvz9Zqjk/77p5lJlENk
      TBj8X8LSyF0jINc/IorpgNgmGEi5uEYFeVdwCCRRm/ukFxB+VdNRonpfwreUvGvp
      GLf/HVEW1jFxMwMKqVsvFJ7otTA8j4mXZLrBqP0ULbTJxSL8n1ZVeSIQgErNj/zo
      WSsDNh1L/ESQbeeANklZrES/ZT06cp8WsI81VrE6xe0ecn6NRq0t+14L/oUCAwEA
      AaNQME4wHQYDVR0OBBYEFCWx90n8T/up2cbUOUng565zQJRYMB8GA1UdIwQYMBaA
      FCWx90n8T/up2cbUOUng565zQJRYMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEL
      BQADggEBAKWGBR9H2/k3g6q998rXCwcQm/5cjpDQ7N96X6W2k5N+rikeDv8GQVAD
      j+fN5QflZ+lmwO9Gw/b1ocCTkPM4xIJ0Ly5CFlrm3Uqer3p/D4Gd5Vn3ko29PD+R
      2vMldAqErBKu4zoUDa51bf/JPaytQEYJxHDhhjAg5/Fo2KDimVzIWhftRqt4Nyui
      KpJnGs/qOxWYk6I8CO/61nEySka03tB/oM4V5wn3Fay2pcAgiUiaJIj1AOT8wiv2
      7rzfXaWyV3fEZRf8HVY3Q8X1hSfzz2Fium1zgxmzIah6W+q4dme8n874aBwY0b9r
      VwOa0UchEZkorNxsF/CW+en0Q+5ULJI=
      -----END CERTIFICATE-----
    #we should use yaml|gpg renderer in real world for securing secrets
    key: |
      -----BEGIN PRIVATE KEY-----
      MIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQDLDLddUVA+VPU7
      rZmqCR7i2V/sUJQwJBL1P6BztQhtvNO321OXXjkbq4FSBD9/7d6yYNcqA6i8Trod
      Y9RpOiQ+uq5YqgUGQT/GtHge5LbKmvFvCDtUHdTuAJw0//gggJTx7JLQ1YG78/Wa
      o5P++6eZSZRDZEwY/F/C0shdIyDXPyKK6YDYJhhIubhGBXlXcAgkUZv7pBcQflXT
      UaJ6X8K3lLxr6Ri3/x1RFtYxcTMDCqlbLxSe6LUwPI+Jl2S6waj9FC20ycUi/J9W
      VXkiEIBKzY/86FkrAzYdS/xEkG3ngDZJWaxEv2U9OnKfFrCPNVaxOsXtHnJ+jUat
      LfteC/6FAgMBAAECgf93xAhkApJSMcp0aPE6Wibdp1ntCZpeyuhjwCPkG+Pc1oQK
      7lNf9TO6Hm8o8ViDHMs+a+B956qfZ9BfhYLEjobSeUpdAev0xIfZrJktn7oxxzKh
      SzmzY+U0fSemG1U/1pgEGoZ+p3DebAdyLwocXFN/Fudc2MwJ8NKLF8kXJqE4U/fz
      JdZ2ewrUuUMlxc+HIBU3Lh3AdxdiyhczbX+/FuFTf8CqYhUvY6EyIHT/RnYsGjlh
      YQyv+g6rvIjhBz9sSeUKFY4d+60fI1Ydnt0LOh9gU8CCIiZ/bWxC8Mq6tynOFH2Z
      /Pk+KmRc2F3F+FFp5raOk7sYeAjtvXKXQq/27HUCgYEA62DrggxzbC/D7XdeNU37
      rBChgZujMsQHZaayNiHBzauYHXonJmCAlZrXuBxXxs0YkMyJqGmgZ3XfCWx1Z0GZ
      0BqGuhzLrHkAGyL0DOkfc0xjHWJw1SC3MRQRyDf8szqRYiNvRodXooSoiqy1z7aI
      0x3EiXkqhCBKVdEqmN3q9gMCgYEA3Na42HZj1EILrIJNe3l1BQkR3ycSHH+HFX0f
      FZZZFI3jDo8yRHOD7iMpqpZmobQkSOd6T0YxAFawP492ix5kV3srzs1U6QQn0cHd
      ODwtzT4TxEKAkjp7n9dyD0+ICyusNVLg8hA9/tvFOfiW+aBa6+lu9397Vs6JghmK
      is7GdtcCgYBbo1ktQYGybgo+qomAAUNpYvQuDJUGD7G9jp+1WYElfJf1afhSBaIq
      HKW9pTM7T+Avh6JUcgMkJIzYE2TF9ZFXpuwrC0j0dgtnhT00xZIcBnJUH15Ea79G
      mUSvCbUVZkjxx0/sSocA29ruWn1SSAOPBfjdeNHnrNakAJIv/74ZVwKBgQC81xv/
      ddVNxyBGZrIs+KI5KRVQNg/FsMNpe7Hd+s+3xNC5PXflksXpF8ZkafHfKrSwZshv
      C32RZY+W+m2K7vUs3VWLpIeMLhKCyn0JvmQIEqJCe/+Hi1z1RPzy3LZfnvvGKVXT
      iFILIhDXzpwDX7z/6IAMQcXNaBWr231dzw1kxwKBgA3CVeAn9RA738x5C79J5YSf
      0PZ+Bc0jH4jg1JdKg0X6u7K2IVIPsQ9lspKvAmz2zuAI81O71FfmkKgBngqoYuRk
      C3J1sG8SzBC2xSMERyw53mYjuBdS4mBN28Wtik9ylvOrfYZi6+YGthNXFuSX+WN9
      aZ1ORBfxliVLoRbE/9rr
      -----END PRIVATE KEY-----
