import pandas as pd
from tabulate import tabulate


class LogAnalyzer:
    columns = ['remote_addr', 'remote_user', 'time_local', 'request', 'status', 'body_bytes_sent', 'http_referer',
               'http_user_agent']

    def __init__(self, csv_file):
        self.csv_file = csv_file
        self.logdf = pd.read_csv(self.csv_file, sep=';', encoding='latin1', parse_dates=['time_local'],
                                 names=self.columns)

    def http_codes(self):
        """
        Group http codes and counts
        :param self.logdf:
        :return: dataframe
        """

        stat = self.logdf[['status', 'request']].groupby('status').count().sort_values('request', ascending=False)

        return stat

    def http_requests(self):
        """
        Group http requests and counts
        :param self.logdf: dataframe
        :return: dataframe
        """
        stat = self.logdf[['request', 'status']].groupby('request').count().sort_values('status', ascending=False)
        return stat

    def total_visitors(self):
        """
        Count total visitors by remote ip address
        :param self.logdf: dataframe
        :return: dataframe
        """

        stat = self.logdf.remote_addr.nunique()
        return stat

    def visitors_hits(self):
        """
        Bonus! Group by ip http requests and counts
        :param self.logdf: dataframe
        :return: dataframe
        """
        stat = self.logdf[['remote_addr', 'status']].groupby('remote_addr').count().sort_values('status',
                                                                                                ascending=False)
        return stat


if __name__ == '__main__':

    # path to csv file
    csv_file = 'kredits.csv'
    analyzer = LogAnalyzer(csv_file)

    print("Unique vistors (by ip): {c}".format(c=analyzer.total_visitors()))

    print("Most visited Urls: ")
    print(tabulate(analyzer.http_requests(), headers='keys', tablefmt='psql'))

    print("HTTP codes: ")
    print(tabulate(analyzer.http_codes(), headers='keys', tablefmt='psql'))

    print("Bonus! Group Unique vistors (by ip): ")
    print(tabulate(analyzer.visitors_hits(), headers='keys', tablefmt='psql'))
